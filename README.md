# DB Manager Library PHP
Una pequeña libreria para manejar la base de datos, hacer consultas, inserts, etc.

### Installation

Descarga o clona el proyecto.
Arbol del paquete:
```
db_manager_library |
                   |--> config.json
                   |--> dbml.php
                   |--> test_en.php
                   |--> test_es.php
                   |--> README.md
```
Para poder utilizar necesitas copiar los archivos:
```cmd
config.json
dbml.php
```
Ahora, hay que cambiar los valores del archivo `config.json`
```php
{
    "server":"localhost",       //Nombre del servidor
    "db_name":"db_manager",     //Nombre de la base de datos
    "user":"root",              //Nombre de usuario
    "pass":""                   //Contraseña de usuario
}
```
Incluir archivo `dbml.php`
```php
include('dbml.php');
```
Crear una instancia de la clase `dbManager`
```php
$dbml = new dbManager(table_name,primary_key);
```
### Funciones principales.
| Método | Descripción |
| ------ | ------ |
| insert() | prepara un insert a la tabla |
| update() | preparar un update a la tabla |
| save() | Ejecutar insert() o update() preparado |
| delete() | Ejecutar delete en la tabla |
| where() | prepara un where unico o anidado (AND) |
| orWhere() | prepara un where unico o anidado (OR) |
| select() | Preparar una consulta basica |
| join() | Prepara un join para la consulta |
| getArray() | Genera un array con los datos de la consulta |
| qy() | Ejecutar consultas directamente |
### Ejemplos.
Para este ejemplo utilizare la table 't_users'.

| Columna | Tipo |
| ------ | ------ |
| id_user | PrimaryKey (INT) |
| name | (varchar) 100 |
| email | (varchar) 100 |
| id_status | ForeingKey (INT) |

Incluyo y creo la instancia de la clase `dbManager`
```php
//Incluyo la librería
include('dbml.php');
/*
*   Creamos una instancia de la clase dbManager,
*   con el nombre de la tabla y primarykey.
*/
$dbml = new dbManager('t_users','id_user');
```
#### Insert.
```php
/*
*   Pasamos los parametros a insertar.
*/
$dbml->insert("Jesus","jlabraibarra@gmail.com",1);
/*
*   La ejecución de save nos devolvera un array con el formato:
*       $array['status'] = true / false
*       $array['msg'] = '' / 'Mensaje de error'
*/
print_r($dbml->save());
/* 
*   En este caso save() nos devuelve:
*       {
*            'status':true,
*            'msg':''
*       }
*/
```

#### Update.
```php
/*
*   Para hacer el update es necesario enviar como parametro
*   el id del registro y un array con los datos a setear.
*   El array debe tener la sintaxis:
*       $values = Array('column1'=>'value1','column2'=>'value2','column3'=>'value3'...);
*   El metodo debe tener la sintaxis:
*       $dbml->update(value_primary_key,array_values);
*/
$values = Array('name'=>"Jesus",'email'=>"jesus@gmail.com",'id_status'=>1);
$dbml->update(2,$values);
/*
*   La ejecución de save nos devolvera un array con el formato:
*       $array['status'] = true / false
*       $array['msg'] = '' / 'Mensaje de error'
*/
print_r($dbml->save());
/* 
*   En este caso save() nos devuelve:
*       {
*            'status':true,
*            'msg':''
*       }
*/
```
#### Delete.
```php
/*
*   Para el metodo delete() unicamente hay que enviar el id del registro
*   La ejecución de delete() nos devolvera un array con el formato:
*       $array['status'] = true / false
*       $array['msg'] = '' / 'Mensaje de error'
*/
print_r($dbml->delete(1));
/* 
*   En este caso save() nos devuelve:
*       {
*            'status':true,
*            'msg':''
*       }
*/
```
#### Select
```php
/*
*   Para el metodo select() opcionalmente podemos determianar las
*   columnas que nos devolvera con la siguiente sintaxis:
*       select('column1', 'column2', 'column3'...);
*   Si se deja vacio, por defecto traera todas las columnas de la
*   tabla.
*/
$dbml->select();
/*
*   El metodo select() solo prepara la sentencia select, para obtener
*   los datos hay que hacer uso del metodo getArray(), este metodo
*   retorna todos los datos en forma de array para su manipulación.
*/
print_r($dbml->getArray());
/* 
*   En este caso nos devuelve:
*       {
*           'id_user':1,
*           'name':'Jesus',
            'email':'jlabraibarra@gmail.com',
            'id_status':1
*       }
*/
//  Seleccionando columnas especificas
$dbml->select('name','email');
print_r($dbml->getArray());
/* 
*   En este caso nos devuelve:
*       {
*           'name':'Jesus',
            'email':'jlabraibarra@gmail.com'
*       }
*/
```

#### where (AND)
```php
/*
*   Consulta condicional WHERE unica o concatenada con "and",  
*        por ejemplo: 
*                where column1 = 1 AND column2 = 1
*   
*   El mètodo consta de tres parametros: columna, operador de comparación 
*   y valor condicional.
*        where($column,$condition,$value);
*/
// Consulta con un unico WHERE: SELECT * FROM t_users WHERE id_user = 1;
/*
*   Preparamos el select para obtener las columnas.
*/
$dbml->select();
/*
*   Adicionamos el metodo where('valor1','condicion','valor2').
*/
$dbml->where('id_user','=',1);
/*
*   Por ultimo obtenemos el resultado de la consulta.
*/
print_r($dbml->getArray());
/*
*   Y obtendremos un array como el siguiente:
*  Array
*  (
*    [0] => Array
*        (
*            [0] => 1
*            [id_user] => 1
*            [1] => Jesus Labra
*            [name] => Jesus Labra
*            [2] => jlabraibarra@gmail.com
*            [email] => jlabraibarra@gmail.com
*            [3] => 1
*            [id_status] => 1
*        )
*
*  )
*/
// Consulta con multiples WHERE: SELECT * FROM t_users WHERE id_user = 1 
//   AND name = 'Jesus';
/*
*   Preparamos el select para obtener las columnas.
*/
$dbml->select();
/*
*  Agregamos el primer where.
*/
$dbml->where('id_user','=',1);
/*
*   Agregamos el segundo where.
*/
$dbml->where('name','=',"Jesus");
/*
*   Por ultimo obtenemos el resultado de la consulta.
*/
print_r($dbml->getArray());
```
#### orWhere
```php
/*
*   Consulta condicional WHERE unica o concatenada con "OR",  
*        por ejemplo: 
*                where column1 = 1 OR column2 = 1
*   
*   El mètodo consta de tres parametros: columna, operador de comparaciòn y valor condicional.
*        where($column,$condition,$value);
*/
/*
// Consulta con un unico WHERE: SELECT * FROM t_users WHERE id_user = 1;
/*
*   Preparamos el select para obtener las columnas.
*/
$dbml->select();
/*
*   Agregamos el metodo orWhere('columna1','condicion','valor').
*/
$dbml->orWhere('id_user','=',1);
/*
*   Por ultimo obtenemos el resultado de la consulta.
*/
print_r($dbml->getArray());


// Consulta con multiples WHERE: 
//      SELECT * FROM t_users WHERE id_user = 1 OR name = 'Jesus';
/*
*   Preparamos el select para obtener las columnas.
*/
$dbml->select();
/*
*   agregamos el primer orWhere().
*/
$dbml->orWhere('email','=',"jesus@gmail.com");
/*
*   Agregamos el segundo or where().
*/
$dbml->orWhere('name','=',"Jesus");
/*
*   Por ultimo obtenemos el resultado de la consulta.
*/
print_r($dbml->getArray());
```
#### Join
```php
/*
*   El metodo join() prepara la sentencia de un JOIN para hacer consulta
*   a dos tablas relacionadas, el metodo consta de tres parametros:
*       join('table2','primary_key','foreing_key');
*   En este caso se usa otra tabla llamada 'c_status' que guarda los status
*   del usuario:
*       - id_status
*       - status
*   En este ejemplo traeremos el nombre y status de los usuarios.
*
*   Primero crearemos un select() de los datos a traer.
*/
$dbml->select('t_users.name','c_status.status');
/*
*   Teniendo el select preparado, agregaremos el join.
*/
$dbml->join('c_status','c_status.id_status','t_users.id_status');
/*
*   Tambien podremos agregar un wehere despues del join si es requerido,
*   y al final ejecutaremos la consulta.
*/
print_r($dbml->getArray());
/*
*   Y obtendremos un array como el siguiente:
*       {
*           'name':'Jesus',
*           'email':'jlabraibarra@gmail.com',
*           'status':'activo'
*       }
*/
```
#### Metodo qy
```php
/*
*     Este metodo se utiliza para escribir consultas directamente
*           este metodo no se puede conbinar con los anteriores
*           lo eh agregado para comodidad de algunas consultas que
*           no sean posibles de realizar con los metodos anteriores
*           a continuación muestro algunos ejemplos.
*/
$dbml->qy('SELECT name AS Nombre FROM t_users');
print_r($dbml->getArray());

$dbml->qy('SELECT * FROM t_users');
print_r($dbml->getArray());

$dbml->qy('SELECT * FROM t_users WHERE id_status = 1');
print_r($dbml->getArray());

$dbml->qy('SELECT * FROM t_users WHERE id_status = 2');
print_r($dbml->getArray());
```