<?php
echo '<center>******************</center>
      <br><center>Test dbManager</center><br>
      <center>Fecha de desarrollo: 2021-02-02</center><br>
      <center>Desarrollador: Jesús Labra</center><br>
      <center>*************************</center><br>
      <hr>';

/*******************************************************************************
* Test dbManager                                                               *
*                                                                              *
* Version: 1.1                                                                 *
* Date:    2021-02-02                                                          *
* Autor:  Jesús Labra                                                          *
*******************************************************************************/

//Include library
include('dbml.php');
//Create a new object
$dbml = new dbManager('t_users','id_user');
/*******************************************************************************
*                              Prueba para insert                              *
*******************************************************************************/
/*
//Nueva insercion en tabla t_users
$dbml->insert("Ernesto","Ernesto@gmail.com",1);
//Ejecutar inserción (devuelve una matriz con dos datos, 'estado' de tipo booleano y
//                'msg' de tipo de cadena,en caso de error devuelve falso y el mensaje
//                de error).
print_r($dbml->save());
*/
/*******************************************************************************
*                              Prueba para Update                              *
*******************************************************************************/
//Actualizar un registro (Se debe de enviar como paremtros el ID del registro y 
//                        un arreglo con los datos a actulizar).
/*
*   El arreglo debe tener la siguiente sintaxis:
*       $values = Array('column1'=>'value1','column2'=>'value2','column3'=>'value3'...);
*
*   El método debe tener la siguiente sintaxis:
*       $dbml->update(value_primary_key,array_values);
*/
/*
$values = Array('name'=>"Jesus",'email'=>"jesus@gmail.com",'password'=>1);
$dbml->update(2,$values);

//  Ejecutamos la actualizacion (devuelve una matriz con dos datos, 'estado' de tipo booleano y
//                'msg' de tipo de cadena,en caso de error devuelve falso y el mensaje
//                de error).

echo('<pre>');
print_r($dbml->save());
echo('</pre>');
*/

/*******************************************************************************
*                              Prueba para Delete                              *
*******************************************************************************/
/*
*   Eliminar un registro de la tabla, se debe enviar el ID del registro a eliminar 
*   de tipo INT.
*
*   En caso de exito o fracaso, devuelve un areglo con 'status' (true o false) 
*   y 'msg' (si es true 'msg' estara vacio, de lo contrario tendra el mensaje
*   de error).
*/
/*
echo('<pre>');
print_r($dbml->delete(1));
echo('</pre>');
*/

/*******************************************************************************
*                              Prueba para select                              *
*******************************************************************************/
/*
*    
*   El metodo 'Select' prepara la consulta, puede enviar el nombre de las columnas
*   que desee visualizar como parametros, de la siguiente manera:
*       select ('column1', 'column2' ...)
*                   ò
*       select ('*')
*
*   Si no declara parametros dentro del metodo por defecto devlvera todas las columnas.
*/
/*
$dbml->select();
echo('<pre>');
print_r($dbml->getArray());
echo('</pre>');
*/

/*******************************************************************************
*                         Prueba para where (AND)                              *
*******************************************************************************/
/*
*   Consulta condicional WHERE unica o concatenada con "and",  
*        por ejemplo: 
*                where column1 = 1 AND column2 = 1
*   
*   El mètodo consta de tres parametros: columna, operador de comparaciòn y valor condicional.
*        where($column,$condition,$value);
*/
/*
// Consulta con un unico WHERE: SELECT * FROM t_users WHERE id_user = 1;
$dbml->select();
$data = $dbml->where('id_user','=',1)->getArray();
echo('<pre>');
print_r($data);
echo('</pre>');

// Consulta con multiples WHERE: SELECT * FROM t_users WHERE id_user = 1 AND name = 'Jesus';
$dbml->select();
$data = $dbml->where('email','=',"Ernesto@gmail.com")->where('name','=',"Ernesto")->getArray();
echo('<pre>');
print_r($data);
echo('</pre>');
*/

/*******************************************************************************
*                          Prueba para where (OR)                              *
*******************************************************************************/
/*
*   Consulta condicional WHERE unica o concatenada con "OR",  
*        por ejemplo: 
*                where column1 = 1 OR column2 = 1
*   
*   El mètodo consta de tres parametros: columna, operador de comparaciòn y valor condicional.
*        where($column,$condition,$value);
*/
/*
// Consulta con un unico WHERE: SELECT * FROM t_users WHERE id_user = 1;
$dbml->select();
$data = $dbml->orWhere('id_user','=',1)->getArray();
echo('<pre>');
print_r($data);
echo('</pre>');

// Consulta con multiples WHERE: SELECT * FROM t_users WHERE id_user = 1 OR name = 'Jesus';
$dbml->select();
$data = $dbml->orWhere('email','=',"Ernesto@gmail.com")->orWhere('name','=',"Jesus")->getArray();
echo('<pre>');
print_r($data);
echo('</pre>');
*/

/*******************************************************************************
*                          Prueba para JOIN                                    *
*******************************************************************************/
/*
*   Este metodo prepara JOIN, por ejemplo:
*           JOIN table2 ON table1.id = table1.id
*
*   El metodo consta de tres parametros: segunda tabla, llave primaria, llave foranea.
*
*/
/*
// La siguiente consulta traera nombre y estatus del usuario.
// Preparamos el select
$data = $dbml->select('t_users.name','c_status.status')->join('c_status','c_status.id_status','t_users.id_status')->getArray();
// Ejecutamos la consulta
echo('<pre>');
print_r($data);
echo('</pre>');
*/

/*******************************************************************************
*                          Prueba para QY                                      *
*******************************************************************************/
/*
*     Este metodo se utiliza para escribir consultas directamente
*           este metodo no se puede conbinar con los anteriores
*           lo eh agregado para comodidad de algunas consultas que
*           no sean posibles de realizar con los metodos anteriores
*           a continuación muestro algunos ejemplos.
*/
/*
$dbml->qy('SELECT name AS Nombre FROM t_users');
echo('<pre>');
print_r($dbml->getArray());
echo('</pre>');
*/

/*
$dbml->qy('SELECT * FROM t_users');
echo('<pre>');
print_r($dbml->getArray());
echo('</pre>');
*/

/*
$dbml->qy('SELECT * FROM t_users WHERE id_status = 1');
echo('<pre>');
print_r($dbml->getArray());
echo('</pre>');
*/

/*
$dbml->qy('SELECT * FROM t_users WHERE id_status = 2');
echo('<pre>');
print_r($dbml->getArray());
echo('</pre>');
*/